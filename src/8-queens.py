def solve(N):

    from satispy import Variable, Cnf
    from satispy.solver import Minisat
    from satispy.io import DimacsCnf

    formula = Cnf()
    var = [[Variable("%d %d" % (i, j)) for j in range(N)] for i in range(N)]
    
    # Observation 1
    for i in range(N):
        clause = Cnf()
        for j in range(N):
            clause |= var[i][j]
        formula &= clause
    for j in range(N):
        clause = Cnf()
        for i in range(N):
            clause |= var[i][j]
        formula &= clause
    
    import itertools
    # Observation 2
    for i in range(N):
        for j1, j2 in itertools.combinations(range(N), 2):
            formula &= -var[i][j1] | -var[i][j2]
    for j in range(N):
        for i1, i2 in itertools.combinations(range(N), 2):
            formula &= -var[i1][j] | -var[i2][j]
    
    # Observation 3
    for i in range(N):
        for j in range(N):
            # south-west
            for k in range(1, N):
                if i + k >= N or j - k < 0:
                    break
                formula &= -var[i][j] | -var[i + k][j - k]
            # south-east
            for k in range(1, N):
                if i + k >= N or j + k >= N:
                    break
                formula &= -var[i][j] | -var[i + k][j + k]

    solver = Minisat()
    solution = solver.solve(formula)

    print('== solution ==')
    for i in range(N):
        for j in range(N):
            if (solution[var[i][j]]):
                print('q', end='')
            else :
                print('.', end='')
        print()    

if __name__ == "__main__":
    solve(8)