def solve(board, n):

    from satispy import Variable, Cnf
    from satispy.solver import Minisat

    var = [[[Variable("d: %d %d %d" % (i, j, k)) for k in range(n)] for j in range(w)] for i in range(h)]
    
    formula = Cnf()

    import itertools
    for i in range(h):
        for j in range(w):
            for k1, k2 in itertools.combinations(range(n), 2):
                formula &= -var[i][j][k1] | -var[i][j][k2]

    dx = [-1, 0, 1, 0]
    dy = [0, -1, 0, 1]
    def neighbor_cells(x, y):
        lis = []
        for i in range(4):
            if 0 <= x + dx[i] < h and 0 <= y + dy[i] < w:
                lis.append((x + dx[i], y + dy[i]))
        return lis

    def exactly_one(lis, k):
        sub = Cnf()
        # at least one
        clause = Cnf()
        for x, y in lis:
            clause |= var[x][y][k]
        sub &= clause

        #at most one
        for p1, p2 in itertools.combinations(lis, 2):
            sub &= -var[p1[0]][p1[1]][k] | -var[p2[0]][p2[1]][k]
        return sub


    for i in range(h):
        for j in range(w):
            if board[i][j] > 0:
                formula &= var[i][j][board[i][j] - 1]
                formula &= exactly_one(neighbor_cells(i, j), board[i][j] - 1)
            else :
                lis = neighbor_cells(i, j)
                for k in range(n):
                    # var[i][j][k] -> exactly two neighbor
                    for pos in itertools.combinations(lis, len(lis) - 1):
                        clause = Cnf()
                        for x, y in pos:
                            clause |= var[x][y][k]
                        formula &= -var[i][j][k] | clause 
                    
                    for pos in itertools.combinations(lis, 3):
                        clause = Cnf()
                        for x, y in pos:
                            clause |= -var[x][y][k]
                        formula &= -var[i][j][k] | clause
                    


    solver = Minisat()
    solution = solver.solve(formula)    

    if solution.success:
        for i in range(h):
            for j in range(w):
                for k in range(n):
                    if solution[var[i][j][k]]:
                        board[i][j] = k + 1

        for i in range(h):
            for j in range(w):
                print("%3d" % board[i][j], end='')
            print()
    else :
        print("no solution") 

if __name__ == "__main__":
    filename = '../number_link/data/nl00'
    f = open(filename, 'r')
    h, w, n = map(int, f.readline().split())
    board = [[0 for i in range(w)] for j in range(h)]
    for i in range(2 * n):
        k, x, y = map(int, f.readline().split())
        board[x][y] = k
    
    print(*board, sep='\n')
    solve(board, n)
    f.close()
