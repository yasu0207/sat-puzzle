def solve(lands):

    from satispy import Variable, Cnf
    from satispy.solver import Minisat

    import itertools
    edges = []

    N = len(lands)

    # check if there is no any land between them
    def can_connect(l1, l2, dir):
        op = 'r' if dir == 'c' else 'c'
        if l1[dir] == l2[dir] and l1[op] == l2[op]:
            return False
        for l3 in lands:
            if l1[dir] == l3[dir] and (l1[op] < l3[op] < l2[op] or l2[op] < l3[op] < l1[op]):
                return False
        return True

    # enumerate all bridges
    for i, j in itertools.combinations(range(N), 2):
        l1 = lands[i]
        l2 = lands[j]
        if l1['r'] == l2['r']:
            if can_connect(l1, l2, 'r'):
                if l1['c'] < l2['c']:
                    edges.append((i, j, 'r'))
                else:
                    edges.append((j, i, 'r'))
        elif l1['c'] == l2['c']:
            if can_connect(l1, l2, 'c'):
                if l1['r'] < l2['r']:
                    edges.append((i, j, 'c'))
                else :
                    edges.append((j, i, 'c'))

    M = len(edges)

    # A set of bridges becomes connected if and only if it has a directed tree containing all vertices.
    # Therefore, the goal is to construct a directed rooted tree.
    formula = Cnf()
    # e[i][0] edge[i][1] = true: i-th bridege is presented 
    e = [[Variable('e %d %d' % (i, j)) for j in range(2)] for i in range(M)]
    # r[i] = true: i-th land becomes the root
    r = [Variable('r %d' % (i)) for i in range(N)]
    # a[i][0] (a[i][1]) = true: orient i-th bridge in forward (reverse) direction 
    a = [[Variable('a %d %d' % (i, j)) for j in range(2)] for i in range(M)]
    # d[i][k] = true: dist(root, i) = k
    d = [[Variable('d %d %d' % (i, j)) for j in range(N)] for i in range(N)]

    # Returns a sub formula that represents "exactly k literals are evaluated to true"
    # Optionally, add some disjunction to each clause
    def exactly_k(lis, k, pref = None):
        sub = Cnf()
        for vars in itertools.combinations(lis, len(lis) - k + 1):
            clause = Cnf()
            for var in vars:
                clause |= var

            if pref:
                sub &= pref | clause
            else :
                sub &= clause
        # at most k
        for vars in itertools.combinations(lis, k + 1):
            clause = Cnf()
            for var in vars:
                clause |= -var

            if pref:
                sub &= pref | clause
            else :
                sub &= clause
        return sub
    
    # Collects all variables corresponding to incident bridges to the land
    def incident_edges(land):
        res = []
        for i, edge in enumerate(edges):
            if land == lands[edge[0]] or land == lands[edge[1]]:
                res.append(e[i][0])
                res.append(e[i][1])
        return res
    
    # for each land with budget k, there are exactly k bridges connecting to it.
    for i, land in enumerate(lands):
        formula &= exactly_k(incident_edges(land), land['n'])

    # Returns true if two bridges does not cross each other
    def cross(e1, e2):
        if e1[2] == e2[2]:
            return False
        if e1[2] == 'r' and lands[e1[0]]['c'] < lands[e2[0]]['c'] < lands[e1[1]]['c'] and lands[e2[0]]['r'] < lands[e1[0]]['r'] < lands[e2[1]]['r']:
            return True
        if e1[2] == 'c' and lands[e1[0]]['r'] < lands[e2[0]]['r'] < lands[e1[1]]['r'] and lands[e2[0]]['c'] < lands[e1[0]]['c'] < lands[e2[1]]['c']:
            return True
        return False

    # bridges do not cross each other
    for i, j in itertools.combinations(range(M), 2):
        if cross(edges[i], edges[j]):
            formula &= -e[i][0] | -e[j][0]
            formula &= -e[i][0] | -e[j][1]
            formula &= -e[i][1] | -e[j][0]
            formula &= -e[i][1] | -e[j][1]
    
    # there is exactly one root
    formula &= exactly_k(r, 1)

    # if i-th bridge is oriented in some direction, this must be presented in the solution
    for i in range(M):
        formula &= -a[i][0] | e[i][0] | e[i][1]
        formula &= -a[i][1] | e[i][0] | e[i][1]
    
    # do not orient both direction
    for i in range(M):
        formula &= -a[i][0] | -a[i][1]
    
    # the distance of the root must be zero
    for i in range(N):
        formula &= -r[i] | d[i][0]
    
    # distance is consistent (\sum_{k}d[i][k] = 1)
    for i in range(N):
        formula &= exactly_k(d[i], 1)

    # if e is oriented from i to j, the distance of j is exactly one plus that of i
    for i in range(M):
        for k in range(N - 1):
            s = edges[i][0]
            t = edges[i][1]
            formula &= -a[i][0] | -d[s][k] | d[t][k + 1]
            formula &= -a[i][1] | -d[t][k] | d[s][k + 1]

    # collects all arcs that go into k-th land
    def ingoing(k):
        lis = []
        for i in range(M):
            if edges[i][0] == k:
                lis.append(a[i][1])
            elif edges[i][1] == k:
                lis.append(a[i][0])
        return lis

    # there is no in-going arc to the root
    for i in range(N):
        for ing in ingoing(i):
            formula &= -r[i] | -ing

    # there is exactly one in-going arc to any non-root node
    for i in range(N):
        formula &= exactly_k(ingoing(i), 1, r[i])

    # ==== cnf is ready  ====
    
    solver = Minisat()
    solution = solver.solve(formula)

    if solution.success:

        # print the solution
        w = max([land['r'] for land in lands]) + 1
        h = max([land['c'] for land in lands]) + 1
        board = [[' ' for j in range(2 * h)] for i in range(2 * w)]

        def connect(x1, y1, x2, y2, lines):
            if x1 == x2:
                for j in range(2 * y1 + 1, 2 * y2):
                    board[2 * x1][j] = '=' if lines == 2 else '-'
            else:
                for i in range(2 * x1 + 1, 2 * x2):
                    board[i][2 * y1] = 'Ⅱ' if lines == 2 else '|'
        
        for i in range(M):
            if solution[e[i][0]] and solution[e[i][1]]:
                connect(lands[edges[i][0]]['r'], lands[edges[i][0]]['c'], lands[edges[i][1]]['r'], lands[edges[i][1]]['c'], 2)
            elif solution[e[i][0]] or solution[e[i][1]]:
                connect(lands[edges[i][0]]['r'], lands[edges[i][0]]['c'], lands[edges[i][1]]['r'], lands[edges[i][1]]['c'], 1)
        for land in lands:
            board[2 * land['r']][2 * land['c']] = land['n']
        
        for row in board:
            for cell in row:
                print(cell, end='')
            print()
    else :
        print("no solution")
                
if __name__ == "__main__":
    
    import sys
    lands = []
    for line in sys.stdin:
        x, y, z = line.split()
        lands.append({'r':int(x), 'c':int(y), 'n':int(z)})

    solve(lands)
