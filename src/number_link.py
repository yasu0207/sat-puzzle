def solve(board, n):

    # very slow, to be improved.

    from satispy import Variable, Cnf
    from satispy.solver import Minisat

    # d[i][j][k]: true if the cell (i, j) is connected to the cell (i + 1, j) with the k-th line
    d = [[[Variable("d: %d %d %d" % (i, j, k)) for k in range(n)] for j in range(w)] for i in range(h)]
    # r[i][j][k]: true if the cell (i, j) is connected to the cell (i, j + 1) with the k-th line
    r = [[[Variable("r: %d %d %d" % (i, j, k)) for k in range(n)] for j in range(w)] for i in range(h)]
    
    formula = Cnf()
    # the (h-1)-th row is used for sentinel
    for j in range(w):
        for k in range(n):
            formula &= -d[h - 1][j][k]
    # the (w-1)-th column is used for sentinel
    for i in range(h):
        for k in range(n):
            formula &= -r[i][w - 1][k]

    def incident_edges(x, y):
        res = []
        res.append(d[x][y])
        res.append(r[x][y])
        if x > 0:
            res.append(d[x - 1][y])
        if y > 0:
            res.append(r[x][y - 1])
        return res

    import itertools
    # for each number cell there is exactly one line go out from this cell
    for i in range(h):
        for j in range(w):
            if board[i][j] == 0:
                continue
            edges = incident_edges(i, j)
            clause = Cnf()
            for e in edges:
                clause |= e[board[i][j] - 1]
            formula &= clause
            for e1, e2 in itertools.combinations(edges, 2):
                formula &= -e1[board[i][j] - 1] | -e2[board[i][j] - 1]

    # for each cell there are no two different lines goes through this cell
    for i in range(h):
        for j in range(w):
            for e1, e2 in itertools.product(incident_edges(i, j), repeat=2):
                for k1, k2 in itertools.combinations(range(n), 2):
                    formula &= -e1[k1] | -e2[k2]

    # for each empty cell, the degree is zero or two (i.e. if a line enter in this cell, it must be go out).
    for i in range(h):
        for j in range(w):
            if board[i][j] != 0:
                continue

            edges = incident_edges(i, j)
            for k in range(n):
                
                #the degree is neither one, three, nor four  
                if len(edges) == 2:
                    #edges[0][k] <=> edges[1][k]
                    formula &= (-edges[0][k] | edges[1][k]) & (edges[0][k] | -edges[1][k])
                if len(edges) == 3:
                    formula &= -edges[0][k] |  edges[1][k] | edges[2][k]
                    formula &=  edges[0][k] | -edges[1][k] | edges[2][k]
                    formula &=  edges[0][k] |  edges[1][k] | -edges[2][k]
                    formula &= -edges[0][k] | -edges[1][k] | -edges[2][k]
                if len(edges) == 4:
                    formula &= -edges[0][k] | -edges[1][k] | -edges[2][k] | -edges[3][k]
                    
                    formula &= -edges[0][k] |  edges[1][k] |  edges[2][k] |  edges[3][k]
                    formula &=  edges[0][k] | -edges[1][k] |  edges[2][k] |  edges[3][k]
                    formula &=  edges[0][k] |  edges[1][k] | -edges[2][k] |  edges[3][k]
                    formula &=  edges[0][k] |  edges[1][k] |  edges[2][k] | -edges[3][k]
                    
                    formula &=  edges[0][k] | -edges[1][k] | -edges[2][k] | -edges[3][k]
                    formula &= -edges[0][k] |  edges[1][k] | -edges[2][k] | -edges[3][k]
                    formula &= -edges[0][k] | -edges[1][k] |  edges[2][k] | -edges[3][k]
                    formula &= -edges[0][k] | -edges[1][k] | -edges[2][k] | edges[3][k]

                    
                    
    solver = Minisat()
    solution = solver.solve(formula)

    def printPic(pic):
        for row in pic:
            for c in row:
                print(c, end='')
            print()
    
    def connect(pic, x1, y1, x2, y2, k):
        if y1 == y2: #vertical case
            for i in range(3):
                pic[4 * x1 + 3 + i][4 * y1 + 2] = str(k + 1)
        if x1 == x2: #horizontal case
            for i in range(3):
                pic[4 * x1 + 2][4 * y1 + 3 + i] = str(k + 1)

    if solution.success:
        pic = [[' ' for j in range(4 * w + 1)] for i in range(4 * h + 1)]    
        for i in range(h + 1):
            for j in range(w + 1):
                pic[4 * i][4 * j] = '+'
        for i in range(h):
            for j in range(w):
                for k in range(n):
                    if solution[d[i][j][k]]:
                        connect(pic, i, j, i + 1, j, k)
                    if solution[r[i][j][k]]:
                        connect(pic, i, j, i, j + 1, k)
        for i in range(h):
            for j in range(w):
                if board[i][j] == 0:
                    pic[4 * i + 2][4 * j + 2] = '+'
                elif 0 < board[i][j] <= 9:
                    pic[4 * i + 2][4 * j + 2] = str(board[i][j])
                else :
                    pic[4 * i + 2][4 * j + 2] = str(board[i][j] // 10)
                    pic[4 * i + 2][4 * j + 3] = str(board[i][j] % 10)                    
        printPic(pic)
    else :
        print("no solution")          

if __name__ == "__main__":
    filename = '../number_link/data/nl02'
    f = open(filename, 'r')
    h, w, n = map(int, f.readline().split())
    board = [[0 for i in range(w)] for j in range(h)]
    for i in range(2 * n):
        k, x, y = map(int, f.readline().split())
        board[x][y] = k
    
    print(*board, sep='\n')
    solve(board, n)
    f.close()
