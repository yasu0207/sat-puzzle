def solve(board):
    h = len(board)
    w = len(board[0])

    from satispy import Variable, Cnf
    from satispy.solver import Minisat

    formula = Cnf()
    # var[i][j]: true if the (i, j) cell is colored with black
    var = [[Variable('%d %d' % (i, j)) for j in range(w)] for i in range(h)]


    def neighbor_cells(x, y):
        res = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                if 0 <= x + i < h and 0 <= y + j < w:
                    res.append((x + i, y + j))
        return res

    import itertools
    # returns a sub formula that represents "exactly k literals are evaluated to true"
    def exactly_k(lis, k):
        sub = Cnf()
        for pos in itertools.combinations(lis, len(lis) - k + 1):
            clause = Cnf()
            for x, y in pos:
                clause |= var[x][y]
            sub &= clause
        # at most k
        for pos in itertools.combinations(lis, k + 1):
            clause = Cnf()
            for x, y in pos:
                clause |= -var[x][y]
            sub &= clause
        return sub

    for i in range(h):
        for j in range(w):
            if board[i][j] != '.':
                formula &= exactly_k(neighbor_cells(i, j), int(board[i][j]))
    
    solver = Minisat()
    solution = solver.solve(formula)

    if solution.success:
        print("== solution ==")
        for i in range(h):
            for j in range(w):
                if solution[var[i][j]]:
                    print('■', end='')
                else :
                    print('□', end='')
            print()
    else :
        print("no solution")
                
if __name__ == "__main__":
    
    import sys
    board = []
    for line in sys.stdin:
        board.append(line.strip())

    print(*board, sep="\n")
    solve(board)